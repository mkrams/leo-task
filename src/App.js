import React from 'react';
import styled from 'styled-components';
import List from './components/List';

function App() {
    return (
        <AppWrapper className="App">
            <List/>
        </AppWrapper>
    );
}

const AppWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export default App;
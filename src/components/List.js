import React, {useState, useEffect} from 'react';
import styled from 'styled-components';

function List() {
    const [users, setUsers] = useState([]);
    const [filteredUsers, setFilteredUsers] = useState([]);

    // Gets data from state or localStorage if available.
    // If there's no data for users in localStorage, it sets it there.
    useEffect(() => {
        const fetchUsers = async () => {
            if (localStorage.getItem('users')) {
                setUsers(JSON.parse(localStorage.getItem('users')));
            } else {
                const response = await fetch('https://jsonplaceholder.typicode.com/users');
                const fetchedUsers = await response.json();

                setUsers(fetchedUsers);
                localStorage.setItem('users', JSON.stringify(fetchedUsers));
            }
        };
        fetchUsers();
    }, []);

    // This search function runs on every key entry and reloads list. It can of course work only for enter key by adding
    // condition so it runs only if "event.keyCode === 13".
    const searchUsers = event => {
        const filterValue = event.target.value.toUpperCase();
        const filteredUsers = users.filter(user => user.name.toUpperCase().indexOf(filterValue) > -1);

        setFilteredUsers(filteredUsers);
    };

    const currentUsers = filteredUsers.length ? filteredUsers : users;

    return (
        <>
            {users.length ?
                <>
                    <ListHeader>
                        <h2>Users List</h2>
                        <InputFilter type="text" id="userFilter" onKeyUp={searchUsers}
                                     placeholder="Search by user name..."/>
                    </ListHeader>
                    <UsersList>
                        {currentUsers.map(user => (
                            <li key={user.id}>
                                <UserId>{user.id}.</UserId>
                                <UserName>{user.name}</UserName>
                                <UserUsername>&#64;{user.username}</UserUsername>
                            </li>
                        ))}
                    </UsersList>
                </> :
                <FetchingData>Fetching data...</FetchingData>}
        </>
    )
}

const ListHeader = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 10px 5px;
    margin: 5px 0;
`;

const UsersList = styled.ol`
    min-width: 400px;
    list-style-type: none;
`;

const UserId = styled.span`
    padding: 0 5px;
    color: #b0b0b0;
    display: inline-block;
    min-width: 25px;
    text-align: right;
`;

const UserUsername = styled.span`
    padding: 0 5px;
    color: #b0b0b0;
    font-size: 12px;
`;

const UserName = styled.span`
    padding: 0 5px;
    font-weight: 500;
`;

const InputFilter = styled.input`
    height: 25px;
    padding: 0 5px;
`;

const FetchingData = styled.span`
    margin: 50px;
`;

export default List;
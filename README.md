## Simple list

To run the app, please install necessary packages:

`npm install`

Then, start the project with:

`npm run start`

To list out linter issues, use:

`npm run lint`

For development purposes, I also had a linter fixer script available:

`npm run linter-fix`